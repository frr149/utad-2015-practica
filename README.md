#Práctica Utad 

Iros a http://hackershelf.com/browse/?popular=1 y seleccionad unos 10 libros de varias temáticas que estén disponibles en formato PDF. Este será vuestro modelo.

Cread dos clases de modelo: Book y Library. El primero represnta un solo libro y el otro un conjunto de libros con varias temáticas. Todas las preguntas que haga una tabla o collectionView (via su data source) las debe de responder el objeto de modelo Library.

a) Haced una tabla con celdas personalizadas y con secciones por temáticas. Deben de mostrar info básica sobre el libro.

b) Haced una collectionView que muestre los mismos datos, pero de forma algo distinta.

c) Hacer un TabBarController que permita elegir entre ver esa info como tabla o collectionView.

d) Crea un controlador con una WebView que muestra un pdf en ella (mira la documentación). (PDFViewController)

e) Crea un controlador que muestra los datos de un libro, pero no el pdf. (BookViewController)

f) Crea una App para iPad con un SplitViewController. A la izquierda muestra el tabBarController. A la derecha, el BookVC con algún botón para crear el PDFVC y hacer un push. Cuando se cambia el libro seleccionado (tanto en tabla como en collectionView) si está el PDFVC a la vista éste se debe de actualizar.

g) Guarda el último libro seleccionado de tal forma que al abrir la app siempre muestre el último.

h) Hazla universal.

i) Desafío para nota: mostrar un pdf en una webView es una cutrez. Usando CocoaPods o submódulos de Git instalad esta librería para PDF (https://github.com/vfr/Reader) y haced los cambios pertinentes a PDFVC.

#Forma de entrega

* Sube tu código a github o bitbucket.
* Añademe al proyecto (mi usuario en ambos es frr149)
* Dame un grito por email (fernando@agbo.biz) por si las moscas.


